// Copyright (c) 2022 by Duguang.IO Inc. All Rights Reserved.
// Author: Ethan Liu
// Date: 2022-06-12 09:07:13

package server

import (
	"context"
	"fmt"
	"io"
	"jianmu-worker-docker/engine"
	"net/http"
	"time"

	"github.com/gorilla/websocket"
	"golang.org/x/sync/errgroup"
)

type Server struct {
	Addr    string
	Cert    string
	Handler http.Handler
	Docker  *engine.Docker
}

func (s Server) ListenAndServe(ctx context.Context) error {
	return s.listenAndServe(ctx)
}

func (s Server) listenAndServe(ctx context.Context) error {
	var g errgroup.Group
	srv := &http.Server{
		Addr: s.Addr,
		// Good practice: enforce timeouts for servers you create!
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
		Handler:      s.handleTerminal(),
	}
	g.Go(func() error {
		<-ctx.Done()
		return srv.Shutdown(context.Background())
	})
	g.Go(func() error {
		return srv.ListenAndServe()
	})
	return g.Wait()
}

func (s Server) handleTerminal() http.Handler {
	mux := http.NewServeMux()
	mux.HandleFunc("/terminal", s.terminal)
	return mux
}

var upgrader = websocket.Upgrader{
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

func (s Server) terminal(w http.ResponseWriter, r *http.Request) {
	// websocket握手
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	defer conn.Close()

	r.ParseForm()
	// 获取容器ID或name
	container := r.Form.Get("container")
	hr, err := s.Docker.Attach(r.Context(), container)
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	defer hr.Close()
	defer func() {
		hr.Conn.Write([]byte("exit\r"))
	}()

	go func() {
		wsWriterCopy(hr.Conn, conn)
	}()
	wsReaderCopy(conn, hr.Conn)
}

func wsWriterCopy(reader io.Reader, writer *websocket.Conn) {
	buf := make([]byte, 8192)
	for {
		nr, err := reader.Read(buf)
		if nr > 0 {
			err := writer.WriteMessage(websocket.BinaryMessage, buf[0:nr])
			if err != nil {
				return
			}
		}
		if err != nil {
			return
		}
	}
}

func wsReaderCopy(reader *websocket.Conn, writer io.Writer) {
	for {
		messageType, p, err := reader.ReadMessage()
		if err != nil {
			return
		}
		if messageType == websocket.TextMessage {
			writer.Write(p)
		}
	}
}
